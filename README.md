Minecraft Server launch/config wrapper (Linux, BSD, MAC and windows)

Usage: mcsv [OPTIONS]

    notes:

          - A m.o.t.d (motd=) with a segmented message will require single quotes ('').
            The Minecraft server will strip the quotes before processing the m.o.t.d.

          - The EULA can also be accepted/declined using the -p/--set flag.

          - mcsv can be run in parallel with a running server using --initiate and then issuing the /reload
            command from a server console or OP (client).

OPTIONS:

    -c, --check
            Check for the current server version; see also --snapshot and --version.

    -d, --datapacks [url/filemane]
            Plus sign (+) separated list of datapack url(s) and/or path(s).

    -f, --force
            Force a (re)download or argument.

    -i, --initiate, --info
            Query, init or (re)configure server and exit.

    -l, --list
            List local server versions, worlds.

    -m, --memory [MB's]
            Set memory allocation in MB's.

            example:
                -m 1024

    -p, --set [setting]=[argument]
            Double quoted (""), plus sign (+) separated list of server properties.

            example:
                -p level-name=myworld
                --set "eula=true+pvp=true+motd='My Minecraft Server'+level-name=myworld"

    --path
             Server path.

    -s, --snapshot
            Latest pre-release server version.

    -v, --version [release version]
            Set server version (e.g. -v 1.16.5); see also --snapshot.

    -x, --gui
            Enable server GUI.
