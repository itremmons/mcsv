#!/usr/bin/env python3
# >= 3.6
#
# Minecraft Server launch/config wrapper (Linux, BSD, MAC and windows)
#
# Usage: mcsv [OPTIONS]
#
#     notes:
#
#           - A m.o.t.d (motd=) with a segmented message will require single quotes ('').
#             The Minecraft server will strip the quotes before processing the m.o.t.d.
#
#           - The EULA can also be accepted/declined using the -p/--set flag.
#
#           - mcsv can be run in parallel with a running server using --initiate and then issuing the /reload
#             command from a server console or OP (client).
#
# OPTIONS:
#
#     -c, --check
#             Check for the current server version; see also --snapshot and --version.
#
#     -d, --datapacks [url/filemane]
#             Plus sign (+) separated list of datapack url(s) and/or path(s).
#
#     -f, --force
#             Force a (re)download or argument.
#
#     -i, --initiate, --info
#             Query, init or (re)configure server and exit.
#
#     -l, --list
#             List local server versions, worlds.
#
#     -m, --memory [MB's]
#             Set memory allocation in MB's.
#
#             example:
#                 -m 1024
#
#     -p, --set [setting]=[argument]
#             Double quoted (""), plus sign (+) separated list of server properties.
#
#             example:
#                 -p level-name=myworld
#                 --set "eula=true+pvp=true+motd='My Minecraft Server'+level-name=myworld"
#
#     --path
#             Server path.
#
#     -s, --snapshot
#             Lateset pre-release server version.
#
#     -v, --version [release version]
#             Set server version; see also --snapshot.
#
#     -x, --gui
#             Enable server GUI.
#
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# You should have received a copy of the GNU General Public License with
# this program. If not, refer to <http://www.gnu.org/licenses/>


import argparse
import fileinput
import hashlib
import json
import os
import re
import sys
import time

from urllib.request import urlopen, Request
from shutil import copy


CONFIG_FILE = "mcsv.config"
SERVER_CFG = "server.properties"
SERVER_EULA = "eula.txt"

BASE_URL = "https://launcher.mojang.com/v1/objects/"
REL_LIST_JSON_URL = "https://launchermeta.mojang.com/mc/game/version_manifest.json"

BACKTRACK = 400
RAM = 1024

def server_properties(arguments, settings, status=False):
    """Change settings in Minecraft server config files.

    parameters:
        - property
            Minecraft server properties.

            arguments:
                - Minecraft server property

        - properties file
            Minecraft server config file.

            arguments:
                - path to Minecraft server config file

        - status
            Status messages.

            arguments:
                - True
                - False/None

    examples:
        - server_properties("level-name=world2","~/minecraft_server/server.properties")

            changes the level (world) to world2.

        - server_properties("eula=True","~/.minecraft_server/eula.txt")

            accepts the End User License Agreement.
    """

    for line in fileinput.input(settings, inplace=True):
        if str(arguments.split("=")[0]).strip() == str(line.split("=")[0]).strip():
            if str(arguments.split("=")[1]).strip() != str(line.split("=")[1]).strip():
                if status:
                    sys.stderr.write(f"[{time.strftime('%X')}] [Server update/WARN]: Overwriting {settings.split(os.path.sep)[-1]} : {line.strip()} --> {arguments}\n")
                line.replace( str(line.split("=")[0]).strip(), str(arguments.split("=")[0]).strip() )
        else:
            print(line, end='')
    else:

        try:

            with open(settings, "a") as f:
                f.write(arguments + "\n")

        except:

            pass


if __name__ == "__main__":

    os_path = (f"{os.path.expanduser('~')}{os.path.sep}minecraft_server{os.path.sep}")

    parser = argparse.ArgumentParser(add_help=True, prefix_chars="-+", prog='mcsv', formatter_class=argparse.RawDescriptionHelpFormatter,description=("Minecraft Server launch/config wrapper (Linux, BSD, MAC and windows)"), epilog='''notes:
    - A m.o.t.d (motd=) with a segmented message will require single quotes ('').
      The Minecraft server will strip the quotes before processing the m.o.t.d.

    - The EULA can also be accepted/declined using the -p/--set flag.

    - mcsv can be run in parallel with a running server using --initiate and then issuing the /reload
      command from a server console or OP (client).
    ''')

    parser.add_argument('-c', '--check', help="check for the current server version; see also --snapshot and --version", action='store_true')
    parser.add_argument('-d', '--datapacks', metavar='[URL/filename]', type=str, help="plus sign (+) separated list of datapack url(s) and/or path(s)")
    parser.add_argument('-f', '--force', help="force a (re)download or argument", action='store_true')
    parser.add_argument('-i', '--initiate', '--info', help="query, init or (re)configure server and exit", action='store_true')
    parser.add_argument('-l', '--list', help="list local server versions, worlds", action='store_true')
    parser.add_argument('-m', '--mem', metavar='[RAM/mem]', type=int, help="set memory allocation in MB's (e.g. -m 2048)")
    parser.add_argument('-p', '--set', metavar='[setting=argument]', help="double quoted (\"\"), plus sign (+) separated list of server properties (e.g. -p motd='My Server')")
    parser.add_argument('--path', type=str, metavar='[PATH]',help="server path")
    parser.add_argument('-s', '--snapshot', help="latest pre-release server version", action='store_true')
    parser.add_argument('-v', '--version', metavar='[version]', help="set server version (e.g. -v 1.16.5); see also --snapshot")
    parser.add_argument('-x', '--gui',  help="enable server GUI", action='store_true')

    args = parser.parse_args()

    args.offline = False
    args.world = "world"
    if not args.mem:
        args.mem = RAM

    if args.path:
        os_path = f"{args.path}" if args.path.endswith(os.path.sep) else f"{args.path}{os.path.sep}"

    if args.list:
        if os.path.exists(os_path):
            print(f"[{time.strftime('%X')}] [Server LIST]: {os_path}")
            for filename in os.listdir(os_path):
                if os.path.exists(f"{os_path}{filename}{os.path.sep}{CONFIG_FILE}"):
                    print(f"[{time.strftime('%X')}] [Server files/REALM]: {filename}")

            if os.path.isdir(os_path):
                print(f"\n[{time.strftime('%X')}] [Server files/LIST]: {os_path}")
                for filename in os.listdir(os_path):
                    if filename.endswith(".jar"):
                        if filename.startswith("minecraft_server."):
                            print(f"[{time.strftime('%X')}] [Server files/JAR]: {filename[17:-4]}")
                        else:
                            print(f"[{time.strftime('%X')}] [Server files/JAR]: {filename[:-4]}")
                    elif filename.endswith(".zip"):
                            print(f"[{time.strftime('%X')}] [Server files/DATAPACK]: {filename}")
                    elif os.path.exists(f"{os_path}{filename}{os.path.sep}level.dat"):
                        if os.path.exists(f"{os_path}{filename}{os.path.sep}region"):
                            print(f"[{time.strftime('%X')}] [Server files/WORLD]: {filename} (world map)")
                        else:
                            print(f"[{time.strftime('%X')}] [Server files/WORLD]: {filename}")
        sys.exit()

    if "eula" in str(args):
        print("\n\thttps://account.mojang.com/documents/minecraft_eula\n")

    if os.path.exists(os_path + CONFIG_FILE):
        with open(os_path + CONFIG_FILE, 'r') as f:
            for line in f:
                if not args.version and not args.check:
                    if str(line.split("=")[0]) == "baseversion":
                        args.version = str(line.split("=")[1]).strip()
                if not args.mem:
                    if str(line.split("=")[0]) == "mem":
                        args.mem = str(line.split("=")[1]).strip()

    if os.path.exists(os_path + SERVER_CFG) :
        with open(os_path + SERVER_CFG, 'r') as f:
            for line in f:
                if str(line.split("=")[0]) == "online-mode":
                    if str(line.split("=")[1]).strip() == "false":
                        args.offline = True
                if str(line.split("=")[0]) == "level-name":
                    args.world = str(line.split("=")[1]).strip()
    if args.set:
        for setting in args.set.split('+'):
            if setting.split('=')[0] == "level-name":
                args.world = str(setting.split('=')[1].strip())

    if len(sys.argv[1:]) == 1 and sys.argv[1] in ("-i", "--info", "--initiate"):
        rel_ver = args.version
    elif not args.offline or args.check:

        try:

            print(f"[{time.strftime('%X')}] [Server version/WARN]: Downloading {REL_LIST_JSON_URL}")

            REL_LIST_JSON = json.load(urlopen(REL_LIST_JSON_URL))
            CUR_REL_VER = REL_LIST_JSON['latest']['release']
            CUR_PRE_VER = REL_LIST_JSON['latest']['snapshot']

            if args.snapshot:
                rel_ver = CUR_PRE_VER
            elif args.version:
                rel_ver = args.version
            else:
                rel_ver = CUR_REL_VER
            if "rc" in rel_ver or "w" in rel_ver:
                rel_type = "snapshot"
            else:
                rel_type = "release"

            count = 0
            while count < BACKTRACK:
                if REL_LIST_JSON['versions'][count]['type'] == rel_type:
                    if REL_LIST_JSON['versions'][count]['id'] == rel_ver:
                        REL_URL = REL_LIST_JSON['versions'][count]['url']
                        rel_ver = REL_LIST_JSON['versions'][count]['id']
                        break
                count += 1

            REL_VER_JSON = urlopen(REL_URL)
            REL_VER = json.loads(REL_VER_JSON.read())

            sha1 = REL_VER['downloads']['server']['sha1']
            url = REL_VER['downloads']['server']['url']
            rel_ver_time = REL_VER['releaseTime']

        except:

           sys.exit(f"[{time.strftime('%X')}] [Server version/ERROR]: {REL_LIST_JSON_URL}\n\n\t{str.title(rel_type)} not available\n")

    else:
        rel_ver = args.version

    jar = f"minecraft_server.{rel_ver}.jar"

    if args.check:
        relt = f"[{time.strftime('%X')}] [Server update/INFO]: {rel_ver_time}"
        if args.version:
            msg = f"{str.title(rel_type)} {rel_ver} available"
        else:
            msg = f"{str.title(rel_type)} {CUR_PRE_VER} available" if args.snapshot else f"{str.title(rel_type)} {CUR_REL_VER} available"
        sys.exit(f"{relt}\n\n\t{msg}\n")
    elif not args.offline and not args.initiate:
        relt = f"[{time.strftime('%X')}] [Server update/INFO]: {rel_ver_time}"
        msg = f"Release {CUR_REL_VER} available"
        if CUR_REL_VER == CUR_PRE_VER:
            print(f"{relt}\n\n\t**New** {msg}\n")
        elif rel_type == "release":
            if rel_ver != CUR_REL_VER:
                print(f"{relt}\n\n\tLatest {msg}\n")
        elif rel_type == "snapshot":
            msg = f"Release {CUR_PRE_VER} available"
            if rel_ver != CUR_PRE_VER:
                print(f"{relt}\n\n\tLatest {msg}\n")
        elif not os.path.isfile(f"{os_path}{os.path.sep}{jar}"):
            print(f"{relt}\n\n\t{msg}\n")

    if os.path.exists(os_path):
        print(f"[{time.strftime('%X')}] [Server config/INFO]: Server path: {os_path}{args.world}")
    else:
        print(f"[{time.strftime('%X')}] [Server config/WARN]: Creating server directory: {os_path}")
        os.makedirs(os_path)

    if not os.path.isfile(os_path + CONFIG_FILE):
        print(f"[{time.strftime('%X')}] [Server config/WARN]: Creating configuration file: {os_path}{CONFIG_FILE}")
        with open(os_path + CONFIG_FILE, "w") as f:
            f.write("baseversion=%s\n" % args.version)
            f.write("mem=%s\n" % RAM)

    if not os.path.isfile(os_path + SERVER_CFG):
        open(os_path + SERVER_CFG, 'a').close()

    if not os.path.isfile(os_path + "eula.txt"):
        with open(os_path + "eula.txt", 'w') as f:
            f.write("#By changing the setting below to TRUE you are indicating your agreement to our EULA (https://account.mojang.com/documents/minecraft_eula).\n#" + time.strftime("%a %b %d %H:%M:%S %Z %Y") + "\neula=false\n")

    try:

        if not os.path.isfile(os_path + jar) or args.force:
            if not args.offline:
                print(f"[{time.strftime('%X')}] [Server update/INFO]: Establishing connection: {url}")
                jar_asset = urlopen(url)
                asset_length = ('%.2fMB' % (float(jar_asset.info()['Content-Length']) / 1048576))
                rel = jar
                if args.snapshot:
                    rel = "pre-release " + jar
                print(f"[{time.strftime('%X')}] [Server update/INFO]: Downloading minecraft server {rel} ({asset_length})")
                JAR_TMP = open(os_path + jar, 'wb')
                JAR_TMP.write(jar_asset.read())
                JAR_TMP.close()
                SHA1_HASH = hashlib.sha1(open(os_path + jar, 'rb').read())
                SHA1_SUM = SHA1_HASH.hexdigest()
                print(f"[{time.strftime('%X')}] [Server update/INFO]: SHA1 SUM")
                if sha1 == SHA1_SUM:
                    print(f"\n\t{sha1} SHA1SUM\n\t{SHA1_SUM} {jar}\n\n\tPASSED\n")
                else:
                    sys.exit(f"\n\t{sha1} SHA1SUM\n\t{SHA1_SUM} {jar}\n\n\tFAILED\n")

    except:

        sys.exit(f"[{time.strftime('%X')}] [Server version/ERROR]: Error downloading assets")

    server_properties(arguments="baseversion=%s" % ( rel_ver ), settings=os_path + CONFIG_FILE, status=True)
    server_properties(arguments="mem=%s" % ( args.mem ), settings=os_path + CONFIG_FILE, status=True)

    if args.set:
        for setting in args.set.split('+'):
            if 'eula' in setting:
                server_properties(arguments=setting, settings=os_path + SERVER_EULA, status=True)
            else:
                server_properties(arguments=setting, settings=os_path + SERVER_CFG, status=True)

    if os.path.exists(os_path + SERVER_CFG) :
        with open(os_path + SERVER_CFG, 'r') as f:
            for line in f:
                if str(line.split("=")[0]) == "online-mode":
                    if str(line.split("=")[1]).strip() == "false":
                        args.offline = True
                    else:
                        args.offline = False

    if args.datapacks:
        for u in args.datapacks.split('+'):
            dpn = u.split(os.path.sep)[-1]

            try:

                if not os.path.exists(f"{os_path}{args.world}{os.path.sep}datapacks{os.path.sep}"):
                    os.makedirs(f"{os_path}{args.world}{os.path.sep}datapacks{os.path.sep}")
                if not os.path.exists(f"{os_path}{args.world}{os.path.sep}datapacks{os.path.sep}{dpn}"):
                    if os.path.exists(u):
                        if not os.path.exists("{os_path}{args.world}{os.path.sep}datapacks"):
                            os.makedirs("{os_path}{args.world}{os.path.sep}datapacks")
                        copy(u,f"{os_path}{args.world}{os.path.sep}datapacks{os.path.sep}{dpn}")
                    else:
                        dp = urlopen(u)
                        DP_TMP = open("{os_path}{args.world}{os.path.sep}datapacks{os.path.sep}{dpn}", 'wb')
                        DP_TMP.write(dp.read())
                        DP_TMP.close()
                    print(f"[{time.strftime('%X')}] [Server config/INFO]: Datapack: installed: {dpn}")

            except:

                print(f"[{time.strftime('%X')}] [Server config/WARN]: Datapack: failed installing: {dpn}")

    if args.offline:
        print(f"[{time.strftime('%X')}] [Server config/WARN]: Offline mode Enabled")

    if args.initiate:
        print(f"[{time.strftime('%X')}] [Server config/INFO]: Server version: {rel_ver}")
        print(f"[{time.strftime('%X')}] [Server config/INFO]: Level name: {args.world}")
        sys.exit()

    msg = f"java -Xmx{args.mem}M -Xms{args.mem}M -jar {jar}"
    cmd = f"{msg}" if args.gui else f"{msg} nogui"
    print(f"[{time.strftime('%X')}] [Server config/EXEC]: {cmd}")

    os.chdir(os_path)
    os.system(cmd)
